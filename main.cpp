#include<iostream>
#include<fstream>
#include<stdio.h>
#include <armadillo>

using namespace std;
using namespace arma;

/*
Program submitted by : Kapil Saraswat
Date:   25 May 2020
Short description: Function similar to 2-D meshgrid in Matlab

meshgrid 2-D grids

Syntax
cube name_of_variable = meshgrid(vector1, vector2)
cube XX = meshgrid(vec x)
cube XX = meshgrid(vec x,vec y)

Description

cube XX = meshgrid(x, y)

Based on the coordinates contained in the passing vectors x and y the function returns 2 dimensional
grid coordinates in a cube, where the cube can be sliced to get matrix X1 and Y1.
X1 is the sliced matrix where each row is a copy of vector x. Similarly, Y1 is the sliced matrix
where each column is a copy of vector y.

cube XX = meshgrid(x)

However this function is same as previous function as, cube XX = meshgrid(x, x).

*/

///function overloading
cube meshgrid(vec);
cube meshgrid(vec, vec);

/// working program; tested
/// Main Example Starts from here!
int main()
  {
    vec x = linspace(1, 3, 3);  /// Initialization of Vector 1
    vec y = linspace(1, 5, 5);  /// Initialization of Vector 2
    cube result1, result2;      /// Initialization of cube where result will store

    result1=meshgrid(x);    /// Calling function with 2 vector
    result2=meshgrid(x,y);    /// Calling function with 2 vector

    mat X1=result1.slice(0);    /// Slicing cube
    mat Y1=result1.slice(1);    /// Slicing cube

    mat X2=result2.slice(0);    /// Slicing cube
    mat Y2=result2.slice(1);    /// Slicing cube

    cout<<"Vector x: \n"<<x<<endl;
    cout<<"Vector y: \n"<<y<<endl;
    cout<<"Passing 2 parameter x, and y : meshgrid(x,y)"<<endl;
    cout<<"X2: \n"<<X2<<endl<<"Y2: \n"<<Y2<<endl<<endl;
    cout<<"Passing 1 parameter x : meshgrid(x)"<<endl;
    cout<<"X1: \n"<<X1<<endl<<"Y1: \n"<<Y1<<endl<<endl;
return 0;
}

/// Submitted Meshgrid function
///function overloading

cube meshgrid(vec A)
{
    vec AA=A;
    vec BB=A;

    //BB=(para==1)?A:B;   ///if else inline

    int sizeof_vecA=AA.n_elem;
    int sizeof_vecB=BB.n_elem;

    cube mgrd=ones(sizeof_vecB, sizeof_vecA,2);

    mat XX = ones(sizeof_vecB, sizeof_vecA);
    mat YY = ones(sizeof_vecB, sizeof_vecA);
    for(int msgrid=0; msgrid<sizeof_vecB; msgrid++)
    {
        XX.row(msgrid)=trans(AA);
        YY.row(msgrid)=YY.row(msgrid)*BB(msgrid);
    }
    mgrd.slice(0)=XX;
    mgrd.slice(1)=YY;
    return mgrd;
    }
cube meshgrid(vec A, vec B)
{
    vec AA=A;
    vec BB=B;

    ///BB=(para==1)?A:B;   ///if else inline

    int sizeof_vecA=AA.n_elem;
    int sizeof_vecB=BB.n_elem;

    cube mgrd=ones(sizeof_vecB, sizeof_vecA,2);

    mat XX = ones(sizeof_vecB, sizeof_vecA);
    mat YY = ones(sizeof_vecB, sizeof_vecA);
    for(int msgrid=0; msgrid<sizeof_vecB; msgrid++)
    {
        XX.row(msgrid)=trans(AA);
        YY.row(msgrid)=YY.row(msgrid)*BB(msgrid);
    }
    mgrd.slice(0)=XX;
    mgrd.slice(1)=YY;
    return mgrd;
}
