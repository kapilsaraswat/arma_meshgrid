# ARMA_meshgrid

armadillo function similar to 2-D meshgrid in Matlab

Syntax
cube name_of_variable = meshgrid(vector1, vector2)
cube XX = meshgrid(vec x)
cube XX = meshgrid(vec x,vec y)

Description

cube XX = meshgrid(x, y)

Based on the coordinates contained in the passing vectors x and y the function returns 2 dimensional
grid coordinates in a cube, where the cube can be sliced to get matrix X1 and Y1.
X1 is the sliced matrix where each row is a copy of vector x. Similarly, Y1 is the sliced matrix
where each column is a copy of vector y.

cube XX = meshgrid(x)

However this function is same as previous function as, cube XX = meshgrid(x, x).